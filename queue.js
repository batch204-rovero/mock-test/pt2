let collection = [];

// Write the queue functions below.

//USAGE OF JS ARRAY METHODS (push, pop, shift, unshift, etc) IS NOT ALLOWED
//Properties (.length) are allowed

function print() {
//display all the items in the array
    return collection;
}

function enqueue(element) {
//add an item in the back of the array
    collection[collection.length] = element;
    return collection;
}

function dequeue() {
//remove an item infront of the array
    for (let i = 1; i < collection.length; i++) { collection[i - 1] = collection[i];
    }
    collection.length--;
    return collection;
}

function front() {
//shows the element at the front
    return collection[0];
}

function size() {
//shows the length of the array
    return collection.length;
}

function isEmpty() {
//shows if the array is empty
    if (collection.length === 0) {
        return true
    } else {
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};